# Comparison of different HDL's

Please note that this project is just for my personal interest in programming languages - specifically to describe maximally
parallel processes in this case. The focus also lies on what those languages allow to express. Verilog, very much like C, does
not make many restrictions and allow for basically all useful and - more importantly - useless or wrong designs/programs. SpinalHDL also
allows for design introspection, like getting the latency of a codepath to use it in the design and has many useful primitives
for rapid development and simple reuse of trusted design.

Every language has a minimal example in a separate folder, including tests and additional notes.
The following HDL's are of interest:

| Name             | Web                                             | Repo                                              | Like  | Notes |
| ---------------- | ----------------------------------------------- | ------------------------------------------------- | ----- | ----- |
| RustHDL          | [rust-hdl.org](https://rust-hdl.org)            | [github](https://github.com/samitbasu/rust-hdl)   | Rust  |       |
| SpinalHDL        | -                                               | [github](https://github.com/SpinalHDL/SpinalHDL)  | Scala |       |
| Chisel           | [chisel-lang.org](https://www.chisel-lang.org/) | [github](https://github.com/chipsalliance/chisel) | Scala |       |
| (System-)Verilog | -                                               | -                                                 | C-ish |       |
